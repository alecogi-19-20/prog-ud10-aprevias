import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Actividad3 {


    public static void main(String[] args) {
        File file = new File("resources" + File.separatorChar + "actividad3", "fichero.txt");

        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write("Alex Coloma Gisbert");
            fileWriter.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }


}
