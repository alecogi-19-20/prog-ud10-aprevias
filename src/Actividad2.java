import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Actividad2 {

    public static void main(String[] args) {
        File file = new File("resources" + File.separatorChar + "actividad2", "fichero.txt");
        try {
            String contenidoFichero = leerFichero(file);
            System.out.println(contenidoFichero);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static String leerFichero(File file) throws IOException {
        StringBuilder contenidoDelFichero = new StringBuilder();
            FileReader fileReader = new FileReader(file);
            do {
                int caracterLeido =  fileReader.read();
                if (caracterLeido < 0) {
                    fileReader.close();
                    return contenidoDelFichero.toString();
                } else {
                    contenidoDelFichero.append((char) caracterLeido);
                }
            } while (true);
    }

}
