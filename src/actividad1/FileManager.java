package actividad1;

import java.io.File;
import java.io.IOException;

public class FileManager {

    public static void main(String[] args) {
        try {
            crearFichero("resources", "example2.txt");
            verFicheros("resources");
            verInformacion("resources", "example2.txt");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Creara un nuevo archivo en el directorio especificado
     *
     * @param ruta
     * @param nombreArchivo
     */
    public static boolean crearFichero(String ruta, String nombreArchivo) throws IOException {
        File directorio = new File(ruta);
        if (directorio.exists() && directorio.isDirectory()) {
            File fichero = new File(directorio, nombreArchivo);
            if (!fichero.exists()) {
                fichero.createNewFile();
                return true;
            }
        }
        return false;
    }

    public static void verFicheros(String directorio) {
        File directorioFile = new File(directorio);
        if (directorioFile.exists() && directorioFile.isDirectory()) {
            String[] files = directorioFile.list();
            for (String name: files) {
                System.out.println(name);
            }
        } else {
            System.out.println("No es un directorio");
        }
    }

    public static void verInformacion(String ruta, String nombreArchivo)  {
        File archivo = new File(ruta, nombreArchivo);
        if (archivo.exists()) {
            System.out.println("Nombre: " + archivo.getName());
            System.out.println("Ruta absoluta: " + archivo.getAbsolutePath());
            System.out.println("Ruta relativa: " + archivo.getPath());
            System.out.println("Se puede leer: " + archivo.canRead());
            System.out.println("Se puede escribir: " + archivo.canWrite());
            System.out.println("Tipo: " + (archivo.isDirectory() ? "Directorio" : "fichero"));
        }
    }
}


















