import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Actividad5 {

    public static void main(String[] args) {
        File originFile = new File("resources" + File.separatorChar
                + "actividad5" + File.separatorChar + "modulos.txt");
        File finalFile = new File("resources" + File.separatorChar
                + "actividad5" + File.separatorChar + "modulos_mayuscula.txt");
        List<String> modulos = null;
        try {
            modulos = leerFichero(originFile);
            escribirFicheroMayusculas(modulos, finalFile);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }
    public static void escribirFicheroMayusculas(List<String> modulos, File file) throws IOException {
        if (!file.exists()) {
            file.createNewFile();
        }
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        for (String item: modulos) {
            bufferedWriter.write(item.toUpperCase());
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
        System.out.println("Fichero creado con éxito");
    }

    public static List<String> leerFichero(File file) throws IOException {
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        ArrayList<String> modulos = new ArrayList<>();
        do {
            String modulo = bufferedReader.readLine();
            if (modulo == null) {
                bufferedReader.close();
                return modulos;
            } else if (!modulo.isEmpty()) {
                modulos.add(modulo);
            }
        } while (true);
    }
}
