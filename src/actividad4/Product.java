package actividad4;

public class Product {

    private int code;

    private String name;

    private int quantity;

    public Product(int codigo, String name, int quantity) {
        this.code = codigo;
        this.name = name;
        this.quantity = quantity;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return String.format("%d,%s,%d", code, name, quantity);
    }
}
