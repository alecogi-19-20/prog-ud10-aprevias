package actividad4;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Actividad4 {

    public static final int CODIGO_TECLADOS = 5;
    public static final int CODIGO_MONITORES = 8;
    public static final int CODIGO_TELEVISORES = 7;
    public static final int FIELD_CODE = 0;
    public static final int FIELD_NAME = 1;
    public static final int FIELD_QUANTITY = 2;

    public static void main(String[] args) {
        File file = new File("resources" + File.separatorChar + "actividad4", "database.txt");
        try {
            List<Product> products = readFileProducts(file);
            showData(products);
        } catch (IOException e) {
            System.out.println("Error leyendo el fichero razon --> " + e.getMessage());
        }
    }

    private static void showData(List<Product> products) {
        for (Product item: products) {
            if (item.getCode() == CODIGO_TELEVISORES) {
                System.out.println("Televisores = " + item.getQuantity());
            } else if (item.getCode() == CODIGO_MONITORES) {
                System.out.println("Monitores = " + item.getQuantity());
            } else if (item.getCode() == CODIGO_TECLADOS) {
                System.out.println("Teclados = " + item.getQuantity());
            }
        }
    }

    public static List<Product> readFileProducts(File fileDataBase) throws IOException {
        ArrayList<Product> products = new ArrayList<>();
        FileReader fileReader = new FileReader(fileDataBase);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        do {
            String register = bufferedReader.readLine();
            if (register == null) {
                bufferedReader.close();
                return products;
            } else if (register.isEmpty()) {
                // Si llemos una línea en blanco ignoramos el registro
                continue;
            }
            String[] campos = register.split(",");
            int code = Integer.parseInt(campos[FIELD_CODE].trim());
            String name = campos[FIELD_NAME].trim();
            int quantity = Integer.parseInt(campos[FIELD_QUANTITY].trim());
            Product product = new Product(code, name, quantity);
            products.add(product);
        } while (true);
    }
}
